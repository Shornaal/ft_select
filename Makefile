# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/07 01:27:33 by tiboitel          #+#    #+#              #
#    Updated: 2015/03/30 21:54:57 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 		 =	ft_select
SRCS 		 = 	main.c \
				core.c \
				inputs.c \
				select_init.c \
				signal.c \
				clist.c \
				clist_write.c \
				utils.c \
				move.c \
				return.c \
				underline.c 
INCLUDES	 =	./includes
SRCSPATH 	 =	./srcs/
LIBFTPATH 	 =  ./libft/
LIBFTINC 	 =  ./libft/includes

CC			 = gcc
CFLAGS		 = -Wall -Werror -Wextra -g
INCLUDES_O	 = -I $(LIBFTINC) -I $(INCLUDES)
INCLUDES_C	 = -L $(LIBFTPATH) -lft -L /usr/lib -ltermcap


SRC			 = $(addprefix $(SRCSPATH), $(SRCS))
OBJS		 = $(SRC:.c=.o)

all:		$(NAME)

$(NAME):	$(OBJS)
			$(CC) -o $(NAME) $(OBJS) $(CFLAGS) $(INCLUDES_C)

%.o: %.c libft/libft.a
			$(CC) -o $@ $(INCLUDES_O) -c $<

libft/libft.a:
			make -C $(LIBFTPATH)

clean:
			make -C $(LIBFTPATH) clean
			rm -rf $(OBJS)

fclean: 	clean
			make -C $(LIBFTPATH) fclean
			rm -rf $(NAME)

re: fclean all

.PHONY: clean fclean re
			
