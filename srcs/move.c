/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/28 19:51:40 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/31 17:23:41 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

t_clist_node	*slct_move_up(t_clist *lst, t_clist_node *node)
{
	slct_ununderline(lst, node);
	node = node->prev;
	tputs(tgoto(tgetstr("cm", NULL), node->x, node->y), 1, ft_putc);
	slct_underline(lst, node);
	return (node);
}

t_clist_node	*slct_move_down(t_clist *lst, t_clist_node *node)
{
	slct_ununderline(lst, node);
	node = node->next;
	tputs(tgoto(tgetstr("cm", NULL), node->x, node->y), 1, ft_putc);
	slct_underline(lst, node);
	return (node);
}

t_clist_node	*slct_select(t_clist *lst, t_clist_node *node)
{
	node->is_sel = (node->is_sel) ? 0 : 1;
	slct_underline(lst, node);
	slct_ununderline(lst, node);
	node = node->next;
	tputs(tgoto(tgetstr("cm", NULL), node->x, node->y), 1, ft_putc);
	slct_underline(lst, node);
	return (node);
}
