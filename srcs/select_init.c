/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/07 18:02:33 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 19:47:18 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

t_slct		*get_slct(void)
{
	static t_slct	*singleton;

	if (!singleton)
	{
		if (!(singleton = (t_slct *)ft_memalloc(sizeof(t_slct))))
			return (NULL);
	}
	return (singleton);
}

int			init_args(t_clist *args, char **argv, int argc)
{
	int		i;

	i = 0;
	while (++i < argc)
		if (!clist_add_tail(args, argv[i]))
			return (-1);
	return (0);
}

int			select_init(char **env)
{
	char		*name_term;
	t_slct		*slct;

	if (!(slct = get_slct()))
		return (0);
	name_term = ft_getenv(env, "TERM");
	if (!(slct->term = (t_termios *)ft_memalloc(sizeof(t_termios))))
		return (0);
	if (tgetent(NULL, name_term) == -1)
		return (0);
	free(name_term);
	if (tcgetattr(0, slct->term) == -1)
		return (0);
	slct->term->c_lflag &= ~(ICANON);
	slct->term->c_lflag &= ~(ECHO);
	slct->term->c_cc[VMIN] = 1;
	slct->term->c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSADRAIN, slct->term) == -1)
		return (0);
	tputs(tgetstr("ti", NULL), 1, ft_putc);
	tputs(tgetstr("vi", NULL), 1, ft_putc);
	return (1);
}

int			ft_select(t_clist *args)
{
	t_clist_node	*node;
	t_slct			*slct;

	signal(SIGQUIT, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGWINCH, signal_handler);
	signal(SIGTSTP, signal_handler);
	signal(SIGCONT, signal_handler);
	signal(SIGTERM, signal_handler);
	if (!(slct = get_slct()))
		return (-1);
	if (!(args->fd = open("/dev/tty", O_RDWR)))
		return (-1);
	slct->elems = args;
	slct->curr_node = args->head;
	node = args->head;
	write_list(args, node);
	node = args->head;
	slct_core_loop(slct->elems, slct->curr_node);
	close(args->fd);
	return (1);
}

void		select_exit(int flag)
{
	t_slct		*slct;

	if (!(slct = get_slct()))
		return ;
	if (tcgetattr(0, slct->term) == -1)
		return ;
	slct->term->c_lflag ^= (ICANON | ECHO);
	if (tcsetattr(0, 0, slct->term) == -1)
		return ;
	if (slct->elems != NULL)
		clist_free(slct->elems);
	free(slct->term);
	free(slct);
	if (flag)
	{
		tputs(tgetstr("ve", NULL), 1, ft_putc);
		tputs(tgetstr("te", NULL), 1, ft_putc);
		tputs(tgetstr("cl", NULL), 1, ft_putc);
	}
	exit(0);
}
