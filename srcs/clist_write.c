/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clist_write.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/13 22:09:33 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 20:05:47 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

static void		term_too_small(t_clist *lst)
{
	tputs(tgoto(tgetstr("cm", NULL), 0, 0), 1, ft_putc);
	tputs(tgetstr("cd", NULL), 1, ft_putc);
	write(lst->fd, "Terminal too small\n", 20);
}

void			set_padding(t_clist *lst, t_clist_node *node)
{
	if ((int)((ft_strlen((char *)(node->content)))) > (int)(tgetnum("co")))
		node->count_line = 1 + ft_strlen((char *)(node->content)) / (tgetnum("co"));
	else
		node->count_line = 1;
	if (lst->size_col < (int)(ft_strlen((char *)(node->content))))
		lst->size_col = ft_strlen((char *)(node->content));
	if (tgetnum("li") <= (lst->line + node->prev->count_line))
	{
		lst->line = 0;
		if (lst->curr_col != 1)
			lst->curr_col++;
		lst->curr_col = lst->curr_col + lst->size_col;
	}
}

int				get_coord(t_clist *lst, t_clist_node *node, int line, int curr_col)
{
	if (node != lst->head)
	{
		line = (line) ? line + node->prev->count_line : 1;
		node->y = line - 1;
	}
	else
	{
		node->y = 0;
		line = 1;
	}
	node->x = curr_col;
	return (line);
}

t_clist_node	*write_line(t_clist *lst, t_clist_node *node)
{
	tputs(tgoto(tgetstr("cm", NULL), node->x, node->y), 1, ft_putc);
	if (node->is_sel)
	{
		tputs(tgetstr("mr", NULL), 1, ft_putc);
		write(lst->fd, (char *)(node->content), ft_strlen((char *)(node->content)));
		tputs(tgetstr("me", NULL), 1, ft_putc);
	}
	else
	{
		write(lst->fd, (char *)(node->content), ft_strlen((char *)(node->content)));
	}
	tputs(tgoto(tgetstr("cm", NULL), node->x, node->y + node->count_line), 1, ft_putc);
	node = node->next;
	return (node);
}

int			write_list(t_clist *lst, t_clist_node *node)
{
	t_slct			*slct;
	char			*name_term;
	if (!(slct = get_slct()))
		return (-1);
	lst->index = -1;
	lst->curr_col = 0;
	lst->size_col = 1;

	if ((name_term = getenv("TERM")) == NULL)
			select_exit(1);
	if (tgetent(NULL, name_term) == -1)
			select_exit(1);
	tputs(tgoto(tgetstr("cm", NULL), 0, 0), 1, ft_putc);
	tputs(tgetstr("cd", NULL), 1, ft_putc);
	node = lst->head;
	while ((size_t)(++lst->index) != lst->count)
	{
		if (tgetnum("li") <= 2)
		{
			term_too_small(lst);
			return (0);
		}
		set_padding(lst, node);
		if (lst->curr_col + lst->size_col > tgetnum("co") && (lst->curr_col != 1))
		{
			term_too_small(lst);
			return (0);
		 } 
		if (lst->line == 0 && lst->curr_col != 1)
			lst->size_col = 0;
		lst->line = get_coord(lst, node, lst->line, lst->curr_col);
		node = write_line(lst, node);
	}
	tputs(tgoto(tgetstr("cm", NULL), node->x, node->y), 1, ft_putc);
	if (node == slct->curr_node)
		slct_underline(lst, slct->curr_node);
	return (1);
}
