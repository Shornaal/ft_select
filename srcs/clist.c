/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clist.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/08 19:11:03 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/02 21:36:51 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

t_clist			*clist_new(void)
{
	t_clist	*lst;

	if (!(lst = (t_clist *)ft_memalloc(sizeof(t_clist))))
		return (NULL);
	lst->count = 0;
	lst->head = NULL;
	lst->tail = NULL;
	lst->index = 0;
	lst->curr_col = 0;
	lst->size_col = 0;
	lst->line = 0;
	return (lst);
}

t_clist_node	*clist_create(t_clist_node *prev, t_clist_node *next,
		void *data, size_t content_size)
{
	t_clist_node	*node;

	if (!(node = (t_clist_node *)ft_memalloc(sizeof(t_clist_node))))
		return (NULL);
	node->prev = prev;
	node->next = next;
	node->content = data;
	node->content_size = content_size;
	node->is_sel = 0;
	node->x = 0;
	node->y = 0;
	node->count_line = 0;
	return (node);
}

t_clist			*clist_add_tail(t_clist *lst, void *data)
{
	t_clist_node	*node;

	if (!lst)
		return (NULL);
	if (!(node = (t_clist_node *)ft_memalloc(sizeof(t_clist_node))))
		return (NULL);
	node->content = data;
	node->content_size = sizeof(*data);
	if (!lst->tail)
	{
		node->prev = node;
		lst->head = node;
		lst->tail = node;
	}
	else
	{
		lst->tail->next = node;
		node->prev = lst->tail;
		lst->tail = node;
	}
	node->next = lst->head;
	lst->head->prev = node;
	lst->count += 1;
	return (lst);
}

void			clist_delete_node(t_clist *lst, t_clist_node *node)
{
	t_clist_node	*curr_node;

	if (lst->count == 1)
	{
		lst->head = NULL;
		lst->tail = NULL;
		free(node);
		free(lst);
		select_exit();
	}
	else
	{
		curr_node = node->next;
		node->prev->next = node->next;
		node->next->prev = node->prev;
	}
	if (lst->tail == node)
		lst->tail = node->prev;
	if (lst->head == node)
		lst->head = node->next;
	lst->count--;
	free(node);
	write_list(lst, curr_node);
	tputs(tgoto(tgetstr("cm", NULL), curr_node->x, curr_node->y), 1, ft_putc);
	slct_underline(lst, curr_node);
	slct_core_loop(lst, curr_node);
}

void			clist_free(t_clist *lst)
{
	t_clist_node	*node;
	int				i;

	i = 0;
	if (lst != NULL && lst->head != NULL)
	{
		if (lst->fd > 0)
			close(lst->fd);
		node = lst->head;
		if (node)
		{
			while ((size_t)++i != lst->count)
			{
				node = node->next;
				free(node->prev);
			}
			free(node);
		}
		free(lst);
	}
}
