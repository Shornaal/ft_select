/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   return.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 18:53:00 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 19:46:35 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	signal_exit(void)
{
	t_slct	*slct;

	if (!(slct = get_slct()))
		return ;
	select_exit(1);
}

void	return_args(t_clist *lst)
{
	int				i;
	t_clist_node	*node;

	i = -1;
	node = lst->head->next;
	tputs(tgetstr("ve", NULL), 1, ft_putc);
	tputs(tgetstr("te", NULL), 1, ft_putc);
	tputs(tgetstr("cl", NULL), 1, ft_putc);
	while ((size_t)(++i) < lst->count)
	{
		if (node->is_sel)
		{
			write(1, (char *)(node->content),
					ft_strlen((char *)(node->content)));
			write(1, " ", 1);
		}
		node = node->next;
	}
}
