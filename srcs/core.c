/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/30 21:54:19 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 19:44:33 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	slct_core_loop(t_clist *lst, t_clist_node *node)
{
	t_slct		*slct;
	char		running;
	char		*buffer;

	if (!(slct = get_slct()))
		return ;
	running = 1;
	while (running)
	{
		tputs(tgetstr("ks", NULL), 1, ft_putc);
		buffer = readkey();
		tputs(tgetstr("ke", NULL), 1, ft_putc);
		if ((lst->index == lst->count) &&  (buffer[2] == 'A'
						|| buffer[2] == 'B' || (buffer[2] == '3'
							&& buffer[3] == '~') || buffer[0] == 127
						|| buffer[0] == ' ' || buffer[0] == 10))
		{
			node = action_handler(lst, node, buffer);
			slct->curr_node = node;	
			if (buffer[3] == '~' || buffer[0] == 127)
				running = 0;
			else if (buffer[0] == 10)
			{
				return_args(lst);
				select_exit(0);
			}
		}
		else if (buffer[0] == 27 && !buffer[1])
			select_exit(1);
	}
	clist_delete_node(slct->elems, slct->curr_node);
}
