/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/08 17:41:11 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 20:14:49 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	signal_handler(int signum)
{
	if (signum == SIGQUIT || signum == SIGINT || signum == SIGTERM)
		signal_exit();
	if (signum == SIGWINCH)
		signal_resize();
	if (signum == SIGCONT)
		signal_cont();
	if (signum == SIGTSTP)
		signal_stop();
}

void	signal_resize(void)
{
	t_slct		*slct;

	if (!(slct = get_slct()))
		return ;
	tputs(tgoto(tgetstr("cm", NULL), 0, 0), 1, ft_putc);
	tputs(tgetstr("cd", NULL), 1, ft_putc);
	if (write_list(slct->elems, slct->elems->head))
	{
		tputs(tgoto(tgetstr("cm", NULL), slct->curr_node->x, slct->curr_node->y), 1,
		ft_putc);
		slct_underline(slct->elems, slct->curr_node);
	}
}

void	signal_cont(void)
{
	t_slct			*slct;
	t_clist_node	*node;

	if (!(slct = get_slct()))
		return ;
	tputs(tgetstr("ti", NULL), 1, ft_putc);
	tputs(tgetstr("ks", NULL), 1, ft_putc);
	tputs(tgetstr("vi", NULL), 1, ft_putc);
	tcsetattr(0, 0, slct->term);
	signal(SIGTSTP, signal_handler);
	node = slct->elems->head;
	if (write_list(slct->elems, node))
	{
		tputs(tgoto(tgetstr("cm", NULL), slct->curr_node->x, slct->curr_node->y), 1,
		ft_putc);
		slct_underline(slct->elems, slct->curr_node);
	}

}

void	signal_stop(void)
{
	char	buffer[2];
	t_slct	*slct;

	ft_bzero(buffer, 2);
	if (isatty(1))
	{
		if (!(slct = get_slct()))
			return ;
		buffer[0] = slct->term->c_cc[VSUSP];
		tputs(tgetstr("te", NULL), 1, ft_putc);
		tputs(tgetstr("ke", NULL), 1, ft_putc);
		tputs(tgetstr("ve", NULL), 1, ft_putc);
		signal(SIGTSTP, SIG_DFL);
		ioctl(0, TIOCSTI, buffer);
	}
}
