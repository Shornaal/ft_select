/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   underline.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/13 21:03:57 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/31 17:34:24 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

void	slct_underline(t_clist *lst, t_clist_node *curr)
{
	tputs(tgetstr("us", NULL), 1, ft_putc);
	if (curr->is_sel)
		tputs(tgetstr("mr", NULL), 1, ft_putc);
	write(lst->fd, (char *)(curr->content), ft_strlen((char *)(curr->content)));
	if (curr->is_sel)
		tputs(tgetstr("me", NULL), 1, ft_putc);
	tputs(tgetstr("ue", NULL), 1, ft_putc);
	tputs(tgoto(tgetstr("cm", NULL), curr->x, curr->y), 1, ft_putc);
}

void	slct_ununderline(t_clist *lst, t_clist_node *curr)
{
	if (curr->is_sel)
		tputs(tgetstr("mr", NULL), 1, ft_putc);
	write(lst->fd, (char *)(curr->content), ft_strlen((char *)(curr->content)));
	if (curr->is_sel)
		tputs(tgetstr("me", NULL), 1, ft_putc);
}
