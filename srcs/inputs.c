/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inputs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/07 19:19:31 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 18:53:37 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

t_clist_node	*action_handler(t_clist *lst, t_clist_node *node, char *buffer)
{
	if (buffer[2] == 'A')
		node = slct_move_up(lst, node);
	else if (buffer[2] == 'B')
		node = slct_move_down(lst, node);
	else if (buffer[2] == 'C' || buffer[2] == 'D')
		return (node);
	else if (buffer[0] == ' ')
		node = slct_select(lst, node);
	return (node);
}

char			*readkey(void)
{
	static char		buffer[9];
	int				bytes_read;

	ft_bzero(buffer, 9);
	bytes_read = read(0, buffer, 8);
	return (buffer);

}
