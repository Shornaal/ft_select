/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 19:37:52 by tiboitel          #+#    #+#             */
/*   Updated: 2015/03/31 16:55:58 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_select.h>

int		main(int argc, char **argv, char **env)
{
	t_clist		*args;

	if (!env || !*env)
	{
		ft_putendl_fd("Error: ft_select need a valid environment to execute",
				2);
		return (-1);
	}
	if (argc == 1)
		return (0);
	if (!select_init(env))
	{
		ft_putendl_fd("Error: ft_select need a valid environment to execute",
				2);
		return (-1);
	}
	args = clist_new();
	if (init_args(args, argv, argc) < 0)
		return (-1);
	ft_select(args);
	return (0);
}
