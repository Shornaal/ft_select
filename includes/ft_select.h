/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/07 17:35:32 by tiboitel          #+#    #+#             */
/*   Updated: 2015/04/03 20:06:14 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include	<libft.h>
# include	<termios.h>
# include	<term.h>
# include	<fcntl.h>
# include	<sys/ioctl.h>

typedef	struct termios	t_termios;

typedef struct			s_clist_node
{
	struct s_clist_node	*next;
	struct s_clist_node	*prev;
	void				*content;
	size_t				content_size;
	int					is_sel;
	int					x;
	int					y;
	int					count_line;
}						t_clist_node;

typedef struct			s_clist
{
	t_clist_node		*head;
	t_clist_node		*tail;
	size_t				count;
	int					index;
	int					curr_col;
	int					size_col;
	int					fd;
	int					line;
}						t_clist;

typedef struct			s_slct
{
	t_termios			*term;
	t_clist				*elems;
	t_clist_node		*curr_node;
}						t_slct;

int						init_args(t_clist *args, char **argv, int argc);
int						ft_select(t_clist *args);
char					*readkey(void);
t_slct					*get_slct(void);
int						select_init(char **env);
void					select_exit(int flag);
int						ft_putc(int c);
void					slct_core_loop(t_clist *lst, t_clist_node *node);
void					slct_underline(t_clist *lst, t_clist_node *curr);
void					slct_ununderline(t_clist *lst, t_clist_node *curr);
void					clist_delete_node(t_clist *lst, t_clist_node *node);
t_clist_node			*slct_move_up(t_clist *lst, t_clist_node *node);
t_clist_node			*slct_move_down(t_clist *lst, t_clist_node *node);
t_clist_node			*slct_select(t_clist *lst, t_clist_node *node);
t_clist_node			*action_handler(t_clist *lst, t_clist_node *node, char *buffer);
/*
 *	List functions.
 */
t_clist					*clist_new(void);
t_clist_node			*clist_create(t_clist_node *prev, t_clist_node *next,\
		void *data, size_t content_size);
t_clist					*clist_add_tail(t_clist *lst, void *data);
void					clist_free(t_clist *lst);
void					return_args(t_clist *list);
void					set_padding(t_clist *, t_clist_node *node);
int						get_coord(t_clist *lst, t_clist_node *node, int line, int curr_col);
int						write_list(t_clist *lst, t_clist_node *node);
t_clist_node			*write_line(t_clist *lst, t_clist_node *node);
/*
 *	Signal handler.
 */
void					signal_handler(int signum);
void					signal_exit(void);
void					signal_resize(void);
void					signal_cont(void);
void					signal_stop();
#endif
